import axios from "axios";

// MEU IP 192.168.15.164
const API_URL = "http://192.168.15.164:3000";

export const api = axios.create({
  baseURL: API_URL,
  withCredentials: false,
  headers: {
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
  },
});
