import { useState } from "react";
import { Alert } from "react-native";
import { api } from "../../data/configApi";
import { Input } from "../../components/Inputs/Input";
import { ModalResult } from "../../components/Modal/Modal";
import { Container, InputArea, Title, TitleArea, Text, CustomButton, CustomButtonText, Loading, CustomButtonAddRemoveText, SubText, CustomButtonAdd, CustomButtonRemove } from "./styles";

export function Home() {
  const [loading, setLoading] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState("");

  const [largura1, setLargura1] = useState("5");
  const [largura2, setLargura2] = useState("5");
  const [largura3, setLargura3] = useState("5");
  const [largura4, setLargura4] = useState("5");

  const [altura1, setAltura1] = useState("2");
  const [altura2, setAltura2] = useState("2");
  const [altura3, setAltura3] = useState("2");
  const [altura4, setAltura4] = useState("2");

  const [porta1, setPorta1] = useState("0");
  const [porta2, setPorta2] = useState("0");
  const [porta3, setPorta3] = useState("0");
  const [porta4, setPorta4] = useState("0");

  const [janela1, setJanela1] = useState("0");
  const [janela2, setJanela2] = useState("0");
  const [janela3, setJanela3] = useState("0");
  const [janela4, setJanela4] = useState("0");

  function handlePortas(type: string, number: number) {
    let total = 0;

    if (number == 1) {
      total = type == "R" ? parseInt(porta1) - 1 : parseInt(porta1) + 1;
      setPorta1(total + "");
    } else if (number == 2) {
      total = type == "R" ? parseInt(porta2) - 1 : parseInt(porta2) + 1;
      setPorta2(total + "");
    } else if (number == 3) {
      total = type == "R" ? parseInt(porta3) - 1 : parseInt(porta3) + 1;
      setPorta3(total + "");
    } else {
      total = type == "R" ? parseInt(porta4) - 1 : parseInt(porta4) + 1;
      setPorta4(total + "");
    }
  }

  function handleJanelas(type: string, number: number) {
    let total = 0;

    if (number == 1) {
      total = type == "R" ? parseInt(janela1) - 1 : parseInt(janela1) + 1;
      setJanela1(total + "");
    } else if (number == 2) {
      total = type == "R" ? parseInt(janela2) - 1 : parseInt(janela2) + 1;
      setJanela2(total + "");
    } else if (number == 3) {
      total = type == "R" ? parseInt(janela3) - 1 : parseInt(janela3) + 1;
      setJanela3(total + "");
    } else {
      total = type == "R" ? parseInt(janela4) - 1 : parseInt(janela4) + 1;
      setJanela4(total + "");
    }
  }

  function handleSend() {
    setLoading(true);
    let validacao = validation();

    if (validacao) {
      const data = {
        paredes: [
          {
            largura: largura1,
            altura: altura1,
            portas: porta1,
            janelas: janela1
          },
          {
            largura: largura2,
            altura: altura2,
            portas: porta2,
            janelas: janela2
          },
          {
            largura: largura3,
            altura: altura3,
            portas: porta3,
            janelas: janela3
          },
          {
            largura: largura4,
            altura: altura4,
            portas: porta4,
            janelas: janela4
          },
        ],
      };

      api
        .post(`/calculadora/area`, data)
        .then((response) => {
          if (response.data.erro) {
            Alert.alert("Erro", response.data.erro);
          } else {
            setShowModal(true);
            setData(response.data);
          }
        })
        .catch((error) => {
          console.log(error);
        });
    }

    setLoading(false);
  }

  function validation() {
    let response = false;

    if (
      largura1 == "" || largura2 == "" || largura3 == "" || largura4 == "" ||
      altura1 == "" || altura2 == "" || altura3 == "" || altura4 == ""
    ) {
      Alert.alert("Erro", "Preencher todos os campos!");
      return false;
    } else {
      response = true;
    }

    if (
      parseFloat(largura1) * parseFloat(altura1) <= 1.0 ||
      parseFloat(largura2) * parseFloat(altura2) <= 1.0 ||
      parseFloat(largura3) * parseFloat(altura3) <= 1.0 ||
      parseFloat(largura4) * parseFloat(altura4) <= 1.0 ||

      parseFloat(largura1) * parseFloat(altura1) > 50.0 ||
      parseFloat(largura2) * parseFloat(altura2) > 50.0 ||
      parseFloat(largura3) * parseFloat(altura3) > 50.0 ||
      parseFloat(largura4) * parseFloat(altura4) > 50.0
    ) {
      Alert.alert("Erro", "Nenhuma parede pode ter menos de 1 m² nem mais de 50 m²");
      return false;
    } else {
      response = true;
    }

    return response;
  }

  return (
    <Container>
      <TitleArea>
        <Title>Calcular Litros de Tintas para Sala</Title>
      </TitleArea>

      {/* PAREDE 1 */}

      <Text>PAREDE 1</Text>
      <InputArea>
        <Input
          type="numeric"
          value={largura1}
          placeholder="Digite a largura (m)"
          onChangeText={(value: string) => setLargura1(value)}
        />
        <Input
          type="numeric"
          value={altura1}
          placeholder="Digite a altura (m)"
          onChangeText={(value: string) => setAltura1(value)}
        />
      </InputArea>

      <SubText>Portas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(porta1) <= 0}
          style={{ opacity: parseInt(porta1) > 0 ? 1 : 0.5 }}
          onPress={() => handlePortas("R", 1)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={porta1} placeholder="0" />

        <CustomButtonAdd onPress={() => handlePortas("A", 1)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      <SubText>Janelas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(janela1) <= 0}
          style={{ opacity: parseInt(janela1) > 0 ? 1 : 0.5 }}
          onPress={() => handleJanelas("R", 1)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={janela1} placeholder="0" />

        <CustomButtonAdd onPress={() => handleJanelas("A", 1)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      {/* PAREDE 2 */}

      <Text>PAREDE 2</Text>
      <InputArea>
        <Input
          type="numeric"
          value={largura2}
          placeholder="Digite a largura (m)"
          onChangeText={(value: string) => setLargura2(value)}
        />
        <Input
          type="numeric"
          value={altura2}
          placeholder="Digite a altura (m)"
          onChangeText={(value: string) => setAltura2(value)}
        />
      </InputArea>

      <SubText>Portas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(porta2) <= 0}
          style={{ opacity: parseInt(porta2) > 0 ? 1 : 0.5 }}
          onPress={() => handlePortas("R", 2)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={porta2} placeholder="0" />

        <CustomButtonAdd onPress={() => handlePortas("A", 2)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      <SubText>Janelas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(janela2) <= 0}
          style={{ opacity: parseInt(janela2) > 0 ? 1 : 0.5 }}
          onPress={() => handleJanelas("R", 2)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={janela2} placeholder="0" />

        <CustomButtonAdd onPress={() => handleJanelas("A", 2)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      {/* PAREDE 3 */}

      <Text>PAREDE 3</Text>
      <InputArea>
        <Input
          type="numeric"
          value={largura3}
          placeholder="Digite a largura (m)"
          onChangeText={(value: string) => setLargura3(value)}
        />
        <Input
          type="numeric"
          value={altura3}
          placeholder="Digite a altura (m)"
          onChangeText={(value: string) => setAltura3(value)}
        />
      </InputArea>

      <SubText>Portas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(porta3) <= 0}
          style={{ opacity: parseInt(porta3) > 0 ? 1 : 0.5 }}
          onPress={() => handlePortas("R", 3)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={porta3} placeholder="0" />

        <CustomButtonAdd onPress={() => handlePortas("A", 3)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      <SubText>Janelas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(janela3) <= 0}
          style={{ opacity: parseInt(janela3) > 0 ? 1 : 0.5 }}
          onPress={() => handleJanelas("R", 3)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={janela3} placeholder="0" />

        <CustomButtonAdd onPress={() => handleJanelas("A", 3)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      {/* PAREDE 4 */}

      <Text>PAREDE 4</Text>
      <InputArea>
        <Input
          type="numeric"
          value={largura4}
          placeholder="Digite a largura (m)"
          onChangeText={(value: string) => setLargura4(value)}
        />
        <Input
          type="numeric"
          value={altura4}
          placeholder="Digite a altura (m)"
          onChangeText={(value: string) => setAltura4(value)}
        />
      </InputArea>

      <SubText>Portas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(porta4) <= 0}
          style={{ opacity: parseInt(porta4) > 0 ? 1 : 0.5 }}
          onPress={() => handlePortas("R", 4)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={porta4} placeholder="0" />

        <CustomButtonAdd onPress={() => handlePortas("A", 4)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      <SubText>Janelas</SubText>
      <InputArea>
        <CustomButtonRemove disabled={parseInt(janela4) <= 0}
          style={{ opacity: parseInt(janela4) > 0 ? 1 : 0.5 }}
          onPress={() => handleJanelas("R", 4)}>
          <CustomButtonAddRemoveText>-</CustomButtonAddRemoveText>
        </CustomButtonRemove>

        <Input editable={false} type="numeric" value={janela4} placeholder="0" />

        <CustomButtonAdd onPress={() => handleJanelas("A", 4)}>
          <CustomButtonAddRemoveText>+</CustomButtonAddRemoveText>
        </CustomButtonAdd>
      </InputArea>

      <CustomButton onPress={handleSend}>
        {loading ? <Loading size="large" color="#121214" /> : <CustomButtonText>ENVIAR</CustomButtonText>}
      </CustomButton>

      <ModalResult
        data={data}
        show={showModal}
        setShow={setShowModal}
      />

    </Container >
  );
}