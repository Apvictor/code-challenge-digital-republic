import styled from "styled-components/native";

export const Container = styled.SafeAreaView`
  padding: 20px 20px;
  background-color: #121214;
`;

export const TitleArea = styled.SafeAreaView`
  align-items: center;
  padding: 20px 0px;
`;

export const Title = styled.Text`
  font-size: 24px;
  color: #FFFFFF;
`;

export const InputArea = styled.SafeAreaView`
  display: flex;
  flex-direction: row;
  gap: 10px;
  margin-right: 10px;
`;

export const Text = styled.Text`
  font-size: 20px;
  color: #FFFFFF;
  padding: 10px 10px;
  text-align: center;
`;

export const SubText = styled.Text`
  font-size: 18px;
  color: #FFFFFF80;
  padding: 10px 10px;
`;

export const CustomButton = styled.TouchableOpacity`
  height: 50px;
  background-color: #FFC107;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  margin: 10px 10px;
  `;

export const CustomButtonAdd = styled.TouchableOpacity`
  height: 50px;
  width: 80px;
  background-color: #28A745;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  margin-left: 10px;
`;

export const CustomButtonRemove = styled.TouchableOpacity`
  height: 50px;
  width: 80px;
  background-color: #DC3545;
  border-radius: 10px;
  justify-content: center;
  align-items: center;
  margin-left: 10px;
`;

export const CustomButtonAddRemoveText = styled.Text`
  font-size: 24px;
  color: #121214;
  font-weight: bold;
`;
export const CustomButtonText = styled.Text`
  font-size: 18px;
  color: #121214;
  font-weight: bold;
`;

export const Loading = styled.ActivityIndicator``;
