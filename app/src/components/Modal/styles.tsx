import styled from "styled-components/native";

export const Modal = styled.Modal``;

export const ModalArea = styled.View`
  flex: 1;
  justify-content: flex-end;
  background-color: rgba(0,0,0,0.8);
`;

export const ModalBody = styled.View`
  background-color: #323238;
  border-top-right-radius: 20px;
  border-top-left-radius: 20px;
  min-height: 300px;
  padding: 20px 20px;
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: #FFF;
  text-align: center;
  margin-bottom: 20px;
`;

export const ModalItem = styled.View`
  border-radius: 10px;
  margin-bottom: 15px;
  padding: 10px;
  background: #FFF;
`;

export const ResultadoArea = styled.View``;

export const ResultadoText = styled.Text`
  font-size: 17px;
  font-weight: bold;
  color: #121214;
`;

export const TintasArea = styled.View`
  flex-direction: row;
`;

export const TintaImagem = styled.Image`
  width: 50px;
  height: 50px;
`;

export const TintaContent = styled.SafeAreaView`
  flex: 1;
  padding: 0px 10px;
  justify-content: center;
`;

export const TintaText = styled.Text`
  font-size: 17px;
  font-weight: bold;
  color: #121214;
`;

export const FinishButton = styled.TouchableOpacity`
  height: 50px;
  background-color: #FFC107;
  justify-content: center;
  align-items: center;
  border-radius: 10px;
`;

export const FinishButtonText = styled.Text`
  font-size: 17px;
  font-weight: bold;
  color: #121214;
`;