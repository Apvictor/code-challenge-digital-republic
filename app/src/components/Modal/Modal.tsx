import { ScrollView } from 'react-native';

import { FinishButton, FinishButtonText, Modal, ModalArea, ModalBody, ModalItem, ResultadoArea, ResultadoText, TintaContent, TintaImagem, TintaText, TintasArea, Title } from './styles';

interface Props {
    show: any;
    setShow: any;
    data: any;
}
export function ModalResult({ show, setShow, data }: Props) {
    return (
        <Modal transparent={true} visible={show} animationType="slide">
            <ModalArea>
                <ModalBody>
                    <ScrollView>
                        <Title>Resultados</Title>

                        {/* RESULTADO */}
                        <ModalItem>
                            <ResultadoArea>
                                <ResultadoText>Área a ser pintada: {data && data.areaParaPintar} m²</ResultadoText>
                                <ResultadoText>Litros por m²: {data.resultado && data.resultado.litrosParaPintar} litros</ResultadoText>
                                <ResultadoText>Total de litros de tinta: {data.resultado && data.resultado.litrosTinta} litros</ResultadoText>
                            </ResultadoArea>
                        </ModalItem>

                        {/* LISTA DE TINTAS */}
                        {data.resultado && data.resultado.latas.map((lata: any, key: any) =>
                            <ModalItem key={key}>
                                <TintasArea key={key}>
                                    {lata.id == 1 && <TintaImagem source={require('../../assets/images/1.jpeg')} />}
                                    {lata.id == 2 && <TintaImagem source={require('../../assets/images/2.jpeg')} />}
                                    {lata.id == 3 && <TintaImagem source={require('../../assets/images/3.jpeg')} />}
                                    {lata.id == 4 && <TintaImagem source={require('../../assets/images/4.jpeg')} />}
                                    <TintaContent key={key}>
                                        <TintaText>Lata de tinta</TintaText>
                                        <TintaText>{lata.litros} Litros</TintaText>
                                    </TintaContent >
                                </TintasArea>
                            </ModalItem>
                        )}

                        <FinishButton onPress={() => setShow(false)}>
                            <FinishButtonText>FINALIZAR</FinishButtonText>
                        </FinishButton>

                    </ScrollView>
                </ModalBody>
            </ModalArea>
        </Modal>
    )
}