import { Field, InputArea } from "./styles";

interface Props {
    type: any;
    value: any;
    editable?: boolean;
    placeholder?: string;
    onChangeText?: (value: any) => void;
}

export function Input({ type, placeholder, editable = true, value, onChangeText }: Props) {
    return (
        <InputArea>
            <Field
                value={value}
                keyboardType={type}
                placeholder={placeholder}
                editable={editable}
                onChangeText={onChangeText}
                placeholderTextColor="#FFFFFF80"
            />
        </InputArea>
    );
}