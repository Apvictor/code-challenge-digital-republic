import styled from "styled-components/native";

export const Label = styled.Text`
    color: #FFFFFF80;
    font-size: 18px;
    padding: 10px;
`;

export const InputArea = styled.View`
    width: 100%;
    height: 50px;
    background-color: #202024;
    border: 1px solid #323238;
    flex-direction: row;
    border-radius: 10px;
    padding-left: 15px;
    align-items: center;
    margin-bottom: 15px;
    margin-left: 10px;
    flex: 1;
`;

export const Field = styled.TextInput`
    font-size: 16px;
    margin-left: 10px;
    color: #FFFFFF;
`;
