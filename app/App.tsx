import { ScrollView, StatusBar } from 'react-native';

import { Home } from './src/screens/Home/Home';

export default function App() {
  return (
    <ScrollView>
      <StatusBar barStyle="light-content" translucent />
      <Home />
    </ScrollView>
  );
}
