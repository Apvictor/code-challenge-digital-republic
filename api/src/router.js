import Router from "@koa/router";

import * as calculadora from "./calculadora/calculadora.js";

export const router = new Router();

/* CALCULADORA */
router.post("/calculadora/area", calculadora.calcularTinta);
