const tintas = [
  {
    id: 1,
    litros: 0.5,
  },
  {
    id: 2,
    litros: 2.5,
  },
  {
    id: 3,
    litros: 3.6,
  },
  {
    id: 4,
    litros: 18,
  },
];

const janela = { largura: 2, altura: 1.2 };
const porta = { largura: 0.8, altura: 1.9 };

export function calcularTinta(ctx) {
  const { paredes } = ctx.request.body;

  try {
    const areaParaPintar = calcularAreaParedes(paredes);

    let litros = calcularMetrosParaLitros(areaParaPintar.areaParaPintar);
    let resultado = calcularLatasDeTintas(litros);

    ctx.status = 200;
    ctx.body = {
      resultado: resultado,
      areaParaPintar: areaParaPintar.areaParaPintar,
      erro: areaParaPintar.erro,
    };
  } catch (error) {
    ctx.status = 500;
    ctx.body = error;
  }
}

function calcularAreaParedes(paredes) {
  let erro = "";
  let area = [];
  let areaTotal = 0;
  let portasJson = [];
  let janelasJson = [];
  let areaParaDescontar = 0;
  let areaPortas = 0;
  let areaJanelas = 0;

  for (let i = 0; i < paredes.length; i++) {
    areaTotal += paredes[i].largura * paredes[i].altura;
    area.push(paredes[i].largura * paredes[i].altura);

    for (let y = 0; y < paredes[i].portas; y++) {
      portasJson.push(porta);

      for (let p = 0; p < portasJson.length; p++) {
        // A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta
        if (paredes[i].altura - portasJson[p].altura < 0.3) {
          erro = `A altura de paredes com porta deve ser, no mínimo, 30 centímetros maior que a altura da porta \n\nAltura Parede: ${paredes[i].altura}\nAltura Porta: ${portasJson[p].altura}`;
        }
      }
    }
    for (let z = 0; z < paredes[i].janelas; z++) {
      janelasJson.push(janela);
    }

    areaPortas = calcularAreaPortas(portasJson);
    areaJanelas = calcularAreaJanelas(janelasJson);
    areaParaDescontar = areaPortas + areaJanelas;

    // O total de área das portas e janelas deve ser no máximo 50% da área de parede
    let area50Porcento = area[i] * 0.5;
    if (
      paredes[i].portas > 0 &&
      paredes[i].janelas > 0 &&
      areaParaDescontar >= area50Porcento
    ) {
      erro = `O total de área das portas e janelas deve ser no máximo 50% da área de parede \n\nÁrea Janela + Porta: ${areaParaDescontar} \nÁrea Parede (50%): ${area50Porcento}`;
    }
  }

  let areaParaPintar = areaTotal - areaParaDescontar;

  return {
    areaParaPintar: parseFloat(areaParaPintar.toFixed(2)),
    erro: erro,
  };
}

function calcularAreaPortas(portas) {
  let area = 0;
  for (let i = 0; i < portas.length; i++) {
    area += portas[i].largura * portas[i].altura;
  }

  return parseFloat(area.toFixed(2));
}

function calcularAreaJanelas(janelas) {
  let area = 0;
  for (let i = 0; i < janelas.length; i++) {
    area += janelas[i].largura * janelas[i].altura;
  }

  return parseFloat(area.toFixed(2));
}

function calcularMetrosParaLitros(areaParaPintar) {
  let litros = areaParaPintar / 5;

  return litros;
}

function calcularLatasDeTintas(litrosParaPintar) {
  let totalPintar = 0;
  let litrosTinta = 0;
  let latas = [];

  while (litrosParaPintar > litrosTinta) {
    totalPintar = parseFloat(litrosParaPintar) - parseFloat(litrosTinta);

    if (totalPintar.toFixed(2) >= 18) {
      litrosTinta += tintas[3].litros;
      latas.push(tintas[3]);
    } else if (totalPintar.toFixed(2) >= 3.6) {
      litrosTinta += tintas[2].litros;
      latas.push(tintas[2]);
    } else if (totalPintar.toFixed(2) >= 2.5) {
      litrosTinta += tintas[1].litros;
      latas.push(tintas[1]);
    } else {
      litrosTinta += tintas[0].litros;
      latas.push(tintas[0]);
    }
  }

  return {
    litrosParaPintar: litrosParaPintar.toFixed(2),
    litrosTinta: litrosTinta.toFixed(2),
    latas: latas,
  };
}
