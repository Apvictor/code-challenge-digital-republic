# DIGITAL REPUBLIC CODE CHALLENGE

## Objetivo

O Objetivo desse desafio é avaliar o conhecimento e capacidade dos candidatos às vagas de programação/desenvolvimento.
O teste pode ser feito por qualquer nível de profissional, contudo o critério de avaliação será conforme a experiencia do candidato.

## Instruções API

1. Clone o repositório em sua máquina;
2. Abrir o terminal na pasta do projeto API;
3. Executar `npm i` para instalar os pacotes node;
4. Após a instalação, executar `npm run start` para executar API.

## Instruções APP

1. Com o repositório já em sua máquina;
2. Abrir o terminal na pasta do projeto APP;
3. Executar `npm i` para instalar os pacotes node;
4. Após a instalação, executar `expo start` para executar APP.

## Dicas

- Para visualizar o APP, instalar o Aplicativo **EXPO GO** em sua dispositivo, **EXPO GO** disponível na Play Store;
- Para testar a API, instalar o Postman em sua máquina para testar os endpoints;
- Para testar a APP, alterar o IP para o seu IP no arquivo **src/data/configApi.ts** no projeto APP.

## Endpoint

`POST` _http://localhost:3000/calculadora/area_

BODY (JSON)

```
{
    "paredes": [
        {
            "largura": 5,
            "altura": 2.20,
            "portas": 1,
            "janelas": 1
        },
        {
            "largura": 5,
            "altura": 2,
            "portas": 0,
            "janelas": 0
        },
        {
            "largura": 5,
            "altura": 2,
            "portas": 0,
            "janelas": 0
        },
        {
            "largura": 5,
            "altura": 2,
            "portas": 0,
            "janelas": 0
        }
    ]
}
```
